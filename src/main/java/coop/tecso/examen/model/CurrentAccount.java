package coop.tecso.examen.model;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import coop.tecso.examen.common.enums.Currency;

@Entity
@Table(name="current_account")
public class CurrentAccount implements Serializable {

	private static final long serialVersionUID = -975560023284258938L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account")
	private Long account;
	@Enumerated(EnumType.STRING)
	private Currency currency;
	private double balance;
	
	@OneToMany(mappedBy = "currentAccount")
	private Set<Movement> movements;
	
	
	public Long getAccount() {
		return account;
	}
	public void setAccount(Long id) {
		this.account = id;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Set<Movement> getMovements() {
		return movements;
	}
	public void setMovements(Set<Movement> movements) {
		this.movements = movements;
	}
	
	
}