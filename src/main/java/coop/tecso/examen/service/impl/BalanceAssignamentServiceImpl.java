package coop.tecso.examen.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.common.enums.MovementType;
import coop.tecso.examen.common.enums.Currency;
import coop.tecso.examen.model.CurrentAccount;
import coop.tecso.examen.model.Movement;
import coop.tecso.examen.repository.CurrentAccountRepository;
import coop.tecso.examen.repository.MovementRepository;
import coop.tecso.examen.service.BalanceAssignamentService;

@Service
public class BalanceAssignamentServiceImpl implements BalanceAssignamentService{
	
	@Autowired
	private MovementRepository movementRepository;
	@Autowired
	private CurrentAccountRepository accountRepository;
	
	@Override
	public Movement applyMovementAccount(CurrentAccount account, Movement movement) {
		
		switch(movement.getType()) {
		case DEBITO:
			return applyDebit(account, movement);
		case CREDITO:
			return applyCredit(account, movement);
		default:
			throw new IllegalArgumentException("No es posible aplicar el movimiento");
		}
		
	}
	
	private Movement applyDebit(CurrentAccount account, Movement movement) {
		
		account.setBalance(account.getBalance() + movement.getAmount());
		accountRepository.save(account);
		
		CurrentAccount currentAccount = new CurrentAccount();
		currentAccount.setAccount(account.getAccount());
		movement.setCurrentAccount(currentAccount);
		
		movementRepository.save(movement);
		
		return movement;
	}
	
	private Movement applyCredit(CurrentAccount account, Movement movement) {
		double balance = account.getBalance() - movement.getAmount();
		
		if(account.getCurrency().equals(Currency.PESO)) {
			if( balance < -1000 )
				throw new IllegalArgumentException("Este movimiento genera un descubierto mayor a 1000 pesos");
			
		}else if(account.getCurrency().equals(Currency.DOLAR)) {
			if( balance < -300 )
				throw new IllegalArgumentException("Este movimiento genera un descubierto mayor a 300 dolares");
			
		}else {
			if( balance < -150 )
				throw new IllegalArgumentException("Este movimiento genera un descubierto mayor a 150 euros");
		}
		
		account.setBalance(balance);
		accountRepository.save(account);
		
		CurrentAccount currentAccount = new CurrentAccount();
		currentAccount.setAccount(account.getAccount());
		movement.setCurrentAccount(currentAccount);
		
		movementRepository.save(movement);
		
		return movement;
	}
}
