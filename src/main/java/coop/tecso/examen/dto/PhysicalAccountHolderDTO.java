package coop.tecso.examen.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PhysicalAccountHolderDTO implements Serializable {
	

	private static final long serialVersionUID = -1854383574061855612L;
	
	@NotEmpty
	private String rut;
	@Size(min = 1, max = 10)
	private String name;
	@Size(min = 1, max = 250)
	private String lastname;
	private int cc;
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getCc() {
		return cc;
	}
	public void setCc(int cc) {
		this.cc = cc;
	}
	
}
