package coop.tecso.examen.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import coop.tecso.examen.common.enums.Currency;

public class CurrentAccountDTO implements Serializable{

	private static final long serialVersionUID = -1854383574061855612L;
	
	private Long account;
	@NotNull
	private Currency currency;
	@NotNull
	private double balance;
	
	
	public Long getAccount() {
		return account;
	}
	public void setAccount(Long account) {
		this.account = account;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
}
