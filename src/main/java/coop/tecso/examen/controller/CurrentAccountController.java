package coop.tecso.examen.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import coop.tecso.examen.repository.CurrentAccountRepository;
import coop.tecso.examen.dto.CurrentAccountDTO;
import coop.tecso.examen.model.CurrentAccount;

@RestController
@RequestMapping("/currentaccount")
public class CurrentAccountController {

	@Autowired
	private CurrentAccountRepository currentRepository;
	
	@GetMapping("/findAll")
	public List<CurrentAccountDTO> findAll(){
		List<CurrentAccountDTO> result = new ArrayList<>();
		for(CurrentAccount entity : currentRepository.findAll()) {
			CurrentAccountDTO dto = new CurrentAccountDTO();
			
			dto.setAccount(entity.getAccount());
			dto.setBalance(entity.getBalance());
			dto.setCurrency(entity.getCurrency());
						
			result.add(dto);
		}
		return result;
	}

	@PostMapping("/")
	public ResponseEntity<CurrentAccount>  createCurrentAccount(@Valid @RequestBody CurrentAccountDTO currentAccountDTO ) {
			
		CurrentAccount currentEntity = new CurrentAccount();			
		currentEntity.setBalance(currentAccountDTO.getBalance());
		currentEntity.setCurrency(currentAccountDTO.getCurrency());
		
		final CurrentAccount newCurrent = currentRepository.save(currentEntity);
		return ResponseEntity.ok(newCurrent);
		
	}
	
	@DeleteMapping("/{account}")
	public Map<String, Boolean> deleteCurrentAccount(@PathVariable("account") Long account) throws Exception {
		CurrentAccount currentEntity = currentRepository.findByAccount(account);
		if(currentEntity.getMovements().size() > 0) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "No es posible eliminar la cuenta. Esta cuenta tiene movimientos asociados.");
		}
		currentRepository.delete(currentEntity);
		
		Map<String, Boolean> response =new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
