package coop.tecso.examen.service;

import coop.tecso.examen.model.CurrentAccount;
import coop.tecso.examen.model.Movement;

public interface BalanceAssignamentService {
	
	public Movement applyMovementAccount(CurrentAccount accoutn, Movement movement);

}
