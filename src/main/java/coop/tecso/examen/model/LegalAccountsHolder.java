package coop.tecso.examen.model;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="legal_account_holder")
public class LegalAccountsHolder extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;
	
	private String rut;
	private String businessname;
	@Temporal(value = TemporalType.DATE)
	private Date foundationyear;
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getBusinessName() {
		return businessname;
	}
	public void setBusinessName(String businessName) {
		this.businessname = businessName;
	}
	public Date getFoundationYear() {
		return foundationyear;
	}
	public void setFoundationYear(Date foundationYear) {
		this.foundationyear = foundationYear;
	}

	
}
