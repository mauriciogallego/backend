package coop.tecso.examen.model;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="physical_account_holder")
public class PhysicalAccountHolder extends AbstractPersistentObject {
	
	private static final long serialVersionUID = -8901155893511467206L;
	
	private String rut;
	private String name;
	private String lastname;
	private int cc;
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getCc() {
		return cc;
	}
	public void setCc(int cc) {
		this.cc = cc;
	}
	
	
}
