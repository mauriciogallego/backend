
#### OBSERVACIONES GENERALES ####

1. En este archivo se encuentran las url del backend tanto del ejercicio 1 como del ejercio 2. 

2. Los repositorios para clonar los códigos son los siguientes:
	
	Backend: https://bitbucket.org/mauriciogallego/backend/src/master/
	Frontend: https://bitbucket.org/mauriciogallego/frontend/src/master/

	Nota: El repositorio Frontend tiene dos ramas, la rama master se utilizaria para las pruebas en local. La rama feature_heroku es utilizada para el despliegue en heroku.

3. Las dos soluciones se desplegaron en un servicios publico, pueden consultarlo directamente en:
	
	El frontend esta desplegado en https://rocky-plains-53344.herokuapp.com/
	El backend esta desplegado en https://cryptic-taiga-66656.herokuapp.com
	
	Nota: Tener en cuenta que la primera invocacion puede demorarse un tiempo hasta que  suban los servicio.

4. Pruebas unitarias: 
	
	Por favor revisar las prubeas unitarias que se alcanzaron a realizar

5. Aunque no estaba contemplado tambien incluyo las url del ejercio 1 en caso de que sea necesario.

=====================================================================================================================================================================================
#### Ejercicio 1 ####

##Titulares persona natural-------
## lista de todos titulares de cuenta fisicos:

	curl -X GET "http://localhost:8080/api/accountholder/physical/findAll"

## lista un titular de cuenta fisicos:

	curl -X GET "http://localhost:8080/api/accountholder/physical/"

## Crear un nuevo titular cuenta:

	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"rut\":\"rut-24435\", \"name\":\"DIEGO\",\"lastname\":\"GIL\",\"cc\":1041567233}" "http://localhost:8080/api/accountholder/physical/" 

## Actualizar un titular cuenta persona fisica:

	curl -X PUT -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"rut\":\"88987\", \"name\":\"DIEGO\",\"lastname\":\"GIL\",\"cc\":1041567233}" "http://localhost:8080/api/accountholder/physical/88987" 

## Eliminar un titular cuenta:

	curl -X DELETE "http://localhost:8080/api/accountholder/physical/88987" 

##Titulares persona juridica-------
## lista de todos titulares de cuenta juridicos:

	curl -X GET "http://localhost:8080/api/accountholder/legal/findAll"

## listar un titular de cuenta juridica:

	curl -X GET "http://localhost:8080/api/accountholder/legal/99872"

## Crear un nuevo titular cuenta:

	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"rut\":\"24435\", \"businessname\":\"Empresa NUEVA\",\"foundationyear\":\"2020-09-01\"}" "http://localhost:8080/api/accountholder/legal/" 


## Actualizar un titular cuenta:

	curl -X PUT -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"rut\":\"99872\", \"businessname\":\"Empresa ACTUALIZADA\",\"foundationyear\":\"2018-9-8\"}" "http://localhost:8080/api/accountholder/legal/99872" 

## Eliminar un titular cuenta:

	curl -X DELETE "http://localhost:8080/api/accountholder/legal/99872" 


=====================================================================================================================================================================================
#### Ejercicio 2 ####

## lista de todos las cuentas corrientes:

	curl -X GET "http://localhost:8080/api/currentaccount/findAll"

## Crear una nueva cuenta corriente:

	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"currency\":\"DOLAR\", \"balance\": 206.98}" "http://localhost:8080/api/currentaccount/" 

## Eliminar una cuenta corriente
	
	curl -X DELETE "http://localhost:8080/api/currentaccount/100" 


## lista de todos los movimientos: No era solicitado pero sirve para ver todos los movimientos en caso de que deseen hacer alguna validacion

	curl -X GET "http://localhost:8080/api/movement/findAll"

## lista de todos los movimientos por cuenta: 

	curl -X GET "http://localhost:8080/api/movement/findByAccount/102"

## Crear un nuevo movimiento debito:
Se dejan varios request para utilizar  3 cuentas diferentes

## N° Cuenta: 100
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"DEBITO\", \"amount\": 1001.06, \"description\":\"Debito a cuenta 100\", \"account\":100 }" "http://localhost:8080/api/movement/" 

## N° Cuenta: 101
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"DEBITO\", \"amount\": 8.4, \"description\":\"Debito a cuenta 101\", \"account\":101 }" "http://localhost:8080/api/movement/" 
## N° Cuenta: 102
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"DEBITO\", \"amount\": 103.0, \"description\":\"Debito a cuenta 102\", \"account\":102 }" "http://localhost:8080/api/movement/" 

## -Crear un nuevo movimiento Credito:

## N° Cuenta: 100
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"CREDITO\", \"amount\": 1000.1, \"description\":\"CREDITO a cuenta 100\", \"account\":100 }" "http://localhost:8080/api/movement/" 

## N° Cuenta: 101
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"CREDITO\", \"amount\": 108.4, \"description\":\"CREDITO a cuenta 101\", \"account\":101 }" "http://localhost:8080/api/movement/" 

## N° Cuenta: 102
	curl -X POST -H "Content-Type:application/json" -H "Accept:application/json" -d "{\"type\":\"CREDITO\", \"amount\": 3013.9, \"description\":\"CREDITO a cuenta 102\", \"account\":102 }" "http://localhost:8080/api/movement/" 