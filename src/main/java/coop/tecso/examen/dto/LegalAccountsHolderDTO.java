package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LegalAccountsHolderDTO implements Serializable {
	
	private static final long serialVersionUID = -8901155893511467206L;
	
	@NotEmpty
	private String rut;
	@Size(min = 1, max = 100)
	private String businessname;
	private Date foundationyear;
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getBusinessname() {
		return businessname;
	}
	public void setBusinessname(String businessname) {
		this.businessname = businessname;
	}
	public Date getFoundationyear() {
		return foundationyear;
	}
	public void setFoundationyear(Date foundationyear) {
		this.foundationyear = foundationyear;
	}
	
	
}
