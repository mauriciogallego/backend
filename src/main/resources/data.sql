INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('AR','ARGENTINA', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('BR','BRASIL', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('UY','URUGUAY', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CH','CHILE', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO physical_account_holder(rut, name, lastname, cc, creation_timestamp, modification_timestamp, version_number)
VALUES ('88797','Andres', 'Perez', 1030987654, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1 );

INSERT INTO physical_account_holder(rut, name, lastname, cc, creation_timestamp, modification_timestamp, version_number)
VALUES ('88987','Felipe', 'Garcia', 1039657483, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1 );

INSERT INTO physical_account_holder(rut, name, lastname, cc, creation_timestamp, modification_timestamp, version_number)
VALUES ('88876','Ricardo', 'Paez', 1049345832, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1 );

INSERT INTO legal_account_holder(rut, businessname, foundationyear, creation_timestamp, modification_timestamp, version_number)
VALUES ('99872','Empresa de aseo A', '2020-01-01', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO legal_account_holder(rut, businessname, foundationyear, creation_timestamp, modification_timestamp, version_number)
VALUES ('99345','Empresa de segurosB', '2010-11-21', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO current_account (account, currency, balance) 
VALUES (100,'PESO', 276.06);

INSERT INTO current_account (account, currency, balance) 
VALUES (101,'EURO', 9.87);

INSERT INTO current_account (account, currency, balance) 
VALUES (102,'DOLAR', 1499.78);

INSERT INTO movement(date, type, description, amount, account, creation_timestamp, modification_timestamp, version_number)
VALUES ('2018-01-30T11:44:37.657','DEBITO', 'Movimiento 1 CTA 100', 200.87, 100, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movement(date, type, description, amount, account, creation_timestamp, modification_timestamp, version_number)
VALUES (CURRENT_TIMESTAMP(),'DEBITO', 'Movimiento 3 CTA 100', 576.17, 100, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movement(date, type, description, amount, account, creation_timestamp, modification_timestamp, version_number)
VALUES ('2020-03-12T01:13:27.897','CREDITO', 'Movimiento 2 CTA 100', 1500.98, 100, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movement(date, type, description, amount, account, creation_timestamp, modification_timestamp, version_number)
VALUES (CURRENT_TIMESTAMP(),'CREDITO','Movimiento 1 ', 500.78, 102, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
