package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import coop.tecso.examen.common.enums.MovementType;

public class MovementDTO implements Serializable{

	private static final long serialVersionUID = -1854383574061855612L;
	
	private Date date;
	@NotNull
	private MovementType type;
	@Size(min = 0, max = 200)
	private String description;
	@NotNull
	private double amount;
	@NotNull
	private long account;
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public MovementType getType() {
		return type;
	}
	public void setType(MovementType type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public long getAccount() {
		return account;
	}
	public void setAccount(long account) {
		this.account = account;
	}
	
}
