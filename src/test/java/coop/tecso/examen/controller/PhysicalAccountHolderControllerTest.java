package coop.tecso.examen.controller;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import coop.tecso.examen.model.PhysicalAccountHolder;
import coop.tecso.examen.repository.PhysicalAccountHolderRepository;
import coop.tecso.examen.service.HolderValidator;

import org.junit.Test;

@RunWith(SpringRunner.class)
@WebMvcTest(PhysicalAccountHolderController.class)
public class PhysicalAccountHolderControllerTest {

	@Autowired
    private MockMvc mvc;
	
	@Autowired
	private PhysicalAccountHolderController controller;
	    
    @MockBean
    private PhysicalAccountHolderRepository myRepository;
    @MockBean
    private HolderValidator holderValidator;
	
	@Test
    public void findAllWithOneResultElement() throws Exception {
    	
       	String name = "OSCAR";
    	String lastname="GALLEGO";
    	int cc = 12222;
    	String rut = "99999";
    	
    	PhysicalAccountHolder physical = new PhysicalAccountHolder();
		physical.setName(name);
		physical.setLastname(lastname);
		physical.setCc(cc);
		physical.setRut(rut);
    	
    	when(myRepository.findAll()).thenReturn(Arrays.asList(physical));
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
        
    	mvc.perform(get(root +"/findAll"))
    							.andDo(print())
    							.andExpect(status().isOk())
    							.andExpect(jsonPath("$", hasSize(1)))
    							.andExpect(jsonPath("$[0].name", is(name)))
    							.andExpect(jsonPath("$[0].lastname", is(lastname)))
    							.andExpect(jsonPath("$[0].cc", is(cc)))
    							.andExpect(jsonPath("$[0].rut", is(rut)))
    							.andReturn();	
    }
	
	@Test
    public void createPhysicalHolderTest() throws Exception {
    	
       	String name = "OSCAR";
    	String lastname="GALLEGO";
    	int cc = 12222;
    	String rut = "99999";
    	
    	PhysicalAccountHolder physical = new PhysicalAccountHolder();
		physical.setName(name);
		physical.setLastname(lastname);
		physical.setCc(cc);
		physical.setRut(rut);
    	
    	when(myRepository.save(any(PhysicalAccountHolder.class))).thenReturn(physical);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(physical);
        
    	mvc.perform(post(root +"/").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isOk())
    							.andExpect(jsonPath("$.name", is(name)))
    							.andExpect(jsonPath("$.lastname", is(lastname)))
    							.andExpect(jsonPath("$.cc", is(cc)))
    							.andExpect(jsonPath("$.rut", is(rut)))
    							.andReturn();	
    }
	
	@Test
    public void errorWhenDeleteEntityThanNoExist() throws Exception {
    	
       	String name = "OSCAR";
    	String lastname="GALLEGO";
    	int cc = 12222;
    	String rut = "99999";
    	
    	PhysicalAccountHolder physical = new PhysicalAccountHolder();
		physical.setName(name);
		physical.setLastname(lastname);
		physical.setCc(cc);
		physical.setRut(rut);
    	
    	when(myRepository.findByRut(anyString())).thenReturn(null);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(physical);
        
    	mvc.perform(delete(root +"/"+ rut).contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isNotFound())
    							.andReturn();	
    }
	
	@Test
    public void errorWhenUpdateEntityThanNoExist() throws Exception {
    	
		String name = "OSCAR";
    	String lastname="GALLEGO";
    	int cc = 12222;
    	String rut = "99999";
    	
    	PhysicalAccountHolder physical = new PhysicalAccountHolder();
		physical.setName(name);
		physical.setLastname(lastname);
		physical.setCc(cc);
		physical.setRut(rut);
    	
    	when(myRepository.findByRut(anyString())).thenReturn(null);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(physical);
        
    	mvc.perform(put(root +"/"+ rut).contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isNotFound())
    							.andReturn();	
    }
}
