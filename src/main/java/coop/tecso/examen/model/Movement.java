package coop.tecso.examen.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import coop.tecso.examen.common.enums.MovementType;


@Entity
@Table(name="movement")
public class Movement extends AbstractPersistentObject {

	private static final long serialVersionUID = -8901155893511467206L;

	private Date date;
	@Enumerated(EnumType.STRING)
	private MovementType type;
	private String description;
	private double amount;
	
	@ManyToOne
    @JoinColumn(name = "account", nullable=false)
	private CurrentAccount currentAccount;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public MovementType getType() {
		return type;
	}
	public void setType(MovementType type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public CurrentAccount getCurrentAccount() {
		return currentAccount;
	}
	public void setCurrentAccount(CurrentAccount currentAccount) {
		this.currentAccount = currentAccount;
	}

	
}
