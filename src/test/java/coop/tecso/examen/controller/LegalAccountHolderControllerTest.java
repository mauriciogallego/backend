package coop.tecso.examen.controller;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import coop.tecso.examen.model.LegalAccountsHolder;
import coop.tecso.examen.repository.LegalAccountsHolderRepository;
import coop.tecso.examen.service.HolderValidator;

import org.junit.Test;

@RunWith(SpringRunner.class)
@WebMvcTest(LegalAccountHolderController.class)
public class LegalAccountHolderControllerTest {

	@Autowired
    private MockMvc mvc;
	
	@Autowired
	private LegalAccountHolderController controller;
	    
    @MockBean
    private LegalAccountsHolderRepository myRepository;
    @MockBean
    private HolderValidator holderValidator;

	@Test
    public void findAllWithOneResultElement() throws Exception {
    	
       	String businessname = "EMPRESA ACME";
    	Date foundationyear= Date.from( Instant.now());
    	String rut = "85444";
    	
    	LegalAccountsHolder legal = new LegalAccountsHolder();		
		legal.setBusinessName(businessname);
		legal.setFoundationYear(foundationyear);
		legal.setRut(rut);
    	
    	when(myRepository.findAll()).thenReturn(Arrays.asList(legal));
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
        
    	mvc.perform(get(root +"/findAll"))
    							.andDo(print())
    							.andExpect(status().isOk())
    							.andExpect(jsonPath("$", hasSize(1)))
    							.andExpect(jsonPath("$[0].businessname", is(businessname)))
    							.andExpect(jsonPath("$[0].rut", is(rut)))
    							.andReturn();	
    }
	
	@Test
    public void createPhysicalHolderTest() throws Exception {
    	
       	String businessname = "EMPRESA ACME";
       	Date foundationyear= Date.from( Instant.now());
    	String rut = "85444";
    	
    	LegalAccountsHolder legal = new LegalAccountsHolder();	
		legal.setBusinessName(businessname);
		legal.setFoundationYear(foundationyear);
		legal.setRut(rut);
    	
    	when(myRepository.save(any(LegalAccountsHolder.class))).thenReturn(legal);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(legal);
        
    	mvc.perform(post(root +"/").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isOk())
    							.andExpect(jsonPath("$.businessName", is(businessname)))
    							.andExpect(jsonPath("$.rut", is(rut)))
    							.andReturn();	
    }
	
	@Test
    public void errorWhenDeleteEntityThanNoExist() throws Exception {
    	
       	String businessname = "EMPRESA ACME";
       	Date foundationyear= Date.from( Instant.now());
    	String rut = "85444";
    	
    	LegalAccountsHolder legal = new LegalAccountsHolder();	
		legal.setBusinessName(businessname);
		legal.setFoundationYear(foundationyear);
		legal.setRut(rut);
    	
    	when(myRepository.findByRut(anyString())).thenReturn(null);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(legal);
        
    	mvc.perform(delete(root +"/"+ rut).contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isNotFound())
    							.andReturn();	
    }
	
	@Test
    public void errorWhenUpdateEntityThanNoExist() throws Exception {
    	
		String businessname = "EMPRESA ACME";
		Date foundationyear= Date.from( Instant.now());
    	String rut = "85444";
    	
    	LegalAccountsHolder legal = new LegalAccountsHolder();	
		legal.setBusinessName(businessname);
		legal.setFoundationYear(foundationyear);
		
		legal.setRut(rut);
    	
    	when(myRepository.findByRut(anyString())).thenReturn(null);
    	
    	String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];
    	String requestJson =  new ObjectMapper().writeValueAsString(legal);
        
    	mvc.perform(put(root +"/"+ rut).contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
      							.andDo(print())
    							.andExpect(status().isNotFound())
    							.andReturn();	
    }

}
