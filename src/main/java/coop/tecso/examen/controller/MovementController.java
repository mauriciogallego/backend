package coop.tecso.examen.controller;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import coop.tecso.examen.repository.CurrentAccountRepository;
import coop.tecso.examen.repository.MovementRepository;
import coop.tecso.examen.service.BalanceAssignamentService;
import coop.tecso.examen.dto.MovementDTO;
import coop.tecso.examen.model.CurrentAccount;
import coop.tecso.examen.model.Movement;

@RestController
@RequestMapping("/movement")
public class MovementController {

	@Autowired
	private MovementRepository movementRepository;
	@Autowired
	private CurrentAccountRepository accountRepository;
	@Autowired
	private BalanceAssignamentService balanceAssignamentService;
	
	@GetMapping("/findAll")
	public List<MovementDTO> findAll(){
		List<MovementDTO> result = new ArrayList<>();
		for(Movement entity : movementRepository.findAll()) {
			MovementDTO dto = new MovementDTO();
						
			dto.setDate(entity.getDate());
			dto.setType(entity.getType());
			dto.setDescription(entity.getDescription());
			dto.setAmount(entity.getAmount());
			dto.setAccount(entity.getCurrentAccount().getAccount());
						
			result.add(dto);
		}
		return result;
	}
	
	@GetMapping("/findByAccount/{account}")
	public List<MovementDTO> findByAccount(@PathVariable("account") Long account){
		List<MovementDTO> result = new ArrayList<>();
		CurrentAccount currentAccount = accountRepository.findByAccount(account);
		for(Movement entity : movementRepository.findByCurrentAccountOrderByDateDesc(currentAccount)) {
			MovementDTO dto = new MovementDTO();
						
			dto.setDate(entity.getDate());
			dto.setType(entity.getType());
			dto.setDescription(entity.getDescription());
			dto.setAmount(entity.getAmount());
			dto.setAccount(entity.getCurrentAccount().getAccount());
						
			result.add(dto);
		}
		return result;
	}

	@PostMapping("/")
	public Map<String, Boolean>  createMovement(@Valid @RequestBody MovementDTO movementDTO ) {
			
		Movement movementEntity = new Movement();
			
		movementEntity.setDate( Date.from( Instant.now() ) );
		movementEntity.setType(movementDTO.getType());
		movementEntity.setDescription(movementDTO.getDescription());
		movementEntity.setAmount(movementDTO.getAmount());
		
	//	CurrentAccount currentAccount = new CurrentAccount();
	//	currentAccount.setAccount(movementDTO.getAccount());
	//	movementEntity.setCurrentAccount(currentAccount);
		
	//	movementRepository.save(movementEntity);
		
		balanceAssignamentService.applyMovementAccount(
				accountRepository.findByAccount(movementDTO.getAccount()), 
				movementEntity);
		
		Map<String, Boolean> response =new HashMap<>();
		response.put("saved", Boolean.TRUE);
		return response;
	}
}
