package coop.tecso.examen.service.impl;

import static org.junit.Assert.*;

import java.time.Instant;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import coop.tecso.examen.model.LegalAccountsHolder;
import coop.tecso.examen.model.PhysicalAccountHolder;
import coop.tecso.examen.repository.LegalAccountsHolderRepository;
import coop.tecso.examen.repository.PhysicalAccountHolderRepository;
import coop.tecso.examen.service.HolderValidator;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackages={"coop.tecso.examen.service"})
public class HolderValidatorImplTest {
	
	@Autowired
	private HolderValidator holderValidation;
	
	@Autowired
    private LegalAccountsHolderRepository legalRepository;
	
	@Autowired
	private PhysicalAccountHolderRepository physicalRepository;
	
	private final String RUT1 ="99999";
	private final String RUT2 ="88888";
	
	@Before
    public void setUp() {
		LegalAccountsHolder account = new LegalAccountsHolder();
		account.setBusinessName("Razon social");
		account.setFoundationYear(Date.from( Instant.now()));
		account.setRut(RUT1);
		legalRepository.save(account);
		
		PhysicalAccountHolder physical = new PhysicalAccountHolder();
		physical.setName("OSCAR");
		physical.setLastname("GALLEGO");
		physical.setCc(12222);
		physical.setRut(RUT2);
		physicalRepository.save(physical);
		
		
	}

	@Test(expected = ResponseStatusException.class)
	public void testErrorWhenPhysicalRutExist() {
		
		holderValidation.validateRut(RUT1);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void testErrorWhenLegalRutExist() {
		
		holderValidation.validateRut(RUT2);
	}

}
