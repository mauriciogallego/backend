package coop.tecso.examen.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import coop.tecso.examen.repository.LegalAccountsHolderRepository;
import coop.tecso.examen.service.HolderValidator;
import coop.tecso.examen.dto.LegalAccountsHolderDTO;
import coop.tecso.examen.model.LegalAccountsHolder;


@RestController
@RequestMapping("/accountholder/legal")
public class LegalAccountHolderController {
	
	@Autowired
	private LegalAccountsHolderRepository legalRepository;
	@Autowired
	private HolderValidator holderValidator;
	
	@GetMapping("/findAll")
	public List<LegalAccountsHolderDTO> findAll(){
		List<LegalAccountsHolderDTO> result = new ArrayList<>();
		for(LegalAccountsHolder entity : legalRepository.findAll()) {
			LegalAccountsHolderDTO dto = new LegalAccountsHolderDTO();
			dto.setRut(entity.getRut());
			dto.setBusinessname(entity.getBusinessName());
			dto.setFoundationyear(entity.getFoundationYear());
			
			result.add(dto);
		}
		return result;
	}
	
	@GetMapping("/{rut}")
	public LegalAccountsHolderDTO getByRut(@PathVariable("rut") String rut){
	
		LegalAccountsHolder entity = legalRepository.findByRut(rut);
		LegalAccountsHolderDTO dto = new LegalAccountsHolderDTO();
		dto.setRut(entity.getRut());
		dto.setBusinessname(entity.getBusinessName());
		dto.setFoundationyear(entity.getFoundationYear());
		return dto;
	}

	@PostMapping("/")
	public ResponseEntity<LegalAccountsHolder>  createLegalHolder(@Valid @RequestBody LegalAccountsHolderDTO legalHolder ) {
		
		holderValidator.validateRut(legalHolder.getRut());		
		LegalAccountsHolder legalEntity = new LegalAccountsHolder();			
		legalEntity.setBusinessName(legalHolder.getBusinessname());
		legalEntity.setFoundationYear(legalHolder.getFoundationyear());
		legalEntity.setRut(legalHolder.getRut());
		
		final LegalAccountsHolder newLegal = legalRepository.save(legalEntity);
		return ResponseEntity.ok(newLegal);
		
	}
	
	@PutMapping("/{rut}")
	public ResponseEntity<LegalAccountsHolder>  updateLegalHolder(@Valid @RequestBody LegalAccountsHolderDTO legalHolder, @PathVariable("rut") String rut) throws Exception {
		LegalAccountsHolder legalEntity = legalRepository.findByRut(rut);
		if(legalEntity == null) {
			
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "la persona juridica a actualizar no existe en el sistema");
		}
		legalEntity.setBusinessName(legalHolder.getBusinessname());
		legalEntity.setFoundationYear(legalHolder.getFoundationyear());
		legalEntity.setRut(legalHolder.getRut());
		
		final LegalAccountsHolder updatedLegal = legalRepository.save(legalEntity);
		return ResponseEntity.ok(updatedLegal);
	}

	@DeleteMapping("/{rut}")
	public Map<String, Boolean> deleteLegalHolder(@PathVariable("rut") String rut) throws Exception {
		LegalAccountsHolder legalEntity = legalRepository.findByRut(rut);
		if(legalEntity == null) {
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "la persona juridica a eliminar no existe en el sistema");
		}
		legalRepository.delete(legalEntity);
		
		Map<String, Boolean> response =new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
