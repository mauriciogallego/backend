package coop.tecso.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import coop.tecso.examen.model.LegalAccountsHolder;

public interface LegalAccountsHolderRepository extends JpaRepository<LegalAccountsHolder, Long> {

	LegalAccountsHolder findByRut(String rut);
}
