package coop.tecso.examen.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.RequestBody;


import coop.tecso.examen.repository.PhysicalAccountHolderRepository;
import coop.tecso.examen.service.HolderValidator;
import coop.tecso.examen.dto.PhysicalAccountHolderDTO;
import coop.tecso.examen.model.PhysicalAccountHolder;


@RestController
@RequestMapping("/accountholder/physical")
public class PhysicalAccountHolderController {
	
	@Autowired
	private PhysicalAccountHolderRepository physicalRepository;
	@Autowired
	private HolderValidator holderValidator;
	
	@GetMapping("/findAll")
	public List<PhysicalAccountHolderDTO> findAll(){
		List<PhysicalAccountHolderDTO> result = new ArrayList<>();
		for(PhysicalAccountHolder entity : physicalRepository.findAll()) {
			PhysicalAccountHolderDTO dto = new PhysicalAccountHolderDTO();
			dto.setRut(entity.getRut());
			dto.setName(entity.getName());
			dto.setLastname(entity.getLastname());
			dto.setCc(entity.getCc());
			
			result.add(dto);
		}
		return result;
	}
	
	@GetMapping("/{rut}")
	public PhysicalAccountHolderDTO getByRut(@PathVariable("rut") String rut){
			
		PhysicalAccountHolder entity = physicalRepository.findByRut(rut);
		PhysicalAccountHolderDTO dto = new PhysicalAccountHolderDTO();
		dto.setRut(entity.getRut());
		dto.setName(entity.getName());
		dto.setLastname(entity.getLastname());
		dto.setCc(entity.getCc());
		
		return dto;
	}
	
	@PostMapping("/")
	public ResponseEntity<PhysicalAccountHolder>  createPhysicalHolder(@Valid @RequestBody PhysicalAccountHolderDTO physicalHolder ) {
		
		holderValidator.validateRut(physicalHolder.getRut());		
		PhysicalAccountHolder physicalEntity = new PhysicalAccountHolder();			
		physicalEntity.setCc(physicalHolder.getCc());
		physicalEntity.setName(physicalHolder.getName());
		physicalEntity.setLastname(physicalHolder.getLastname());
		physicalEntity.setRut(physicalHolder.getRut());
		
		final PhysicalAccountHolder newPhysical = physicalRepository.save(physicalEntity);
		return ResponseEntity.ok(newPhysical);
		
	}
	
	@PutMapping("/{rut}")
	public ResponseEntity<PhysicalAccountHolder>  updatePhysicalHolder(@Valid @RequestBody PhysicalAccountHolderDTO physicalHolder, @PathVariable("rut") String rut) throws Exception {
		PhysicalAccountHolder physicalEntity = physicalRepository.findByRut(rut);
		if(physicalEntity == null) {
			
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "la persona natural a actualizar no existe en el sistema");
		}
		physicalEntity.setCc(physicalHolder.getCc());
		physicalEntity.setName(physicalHolder.getName());
		physicalEntity.setLastname(physicalHolder.getLastname());
		physicalEntity.setRut(physicalHolder.getRut());
		
		final PhysicalAccountHolder updatedPhysical = physicalRepository.save(physicalEntity);
		return ResponseEntity.ok(updatedPhysical);
	}

	@DeleteMapping("/{rut}")
	public Map<String, Boolean> deletePhysicalHolder(@PathVariable("rut") String rut) throws Exception {
		PhysicalAccountHolder physicalEntity = physicalRepository.findByRut(rut);
		if(physicalEntity == null) {
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "la persona natural a eliminar no existe en el sistema");
		}
		physicalRepository.delete(physicalEntity);
		
		Map<String, Boolean> response =new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
}
