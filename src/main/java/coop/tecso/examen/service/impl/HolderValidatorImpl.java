package coop.tecso.examen.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import coop.tecso.examen.model.LegalAccountsHolder;
import coop.tecso.examen.model.PhysicalAccountHolder;
import coop.tecso.examen.repository.LegalAccountsHolderRepository;
import coop.tecso.examen.repository.PhysicalAccountHolderRepository;
import coop.tecso.examen.service.HolderValidator;

@Service
public class HolderValidatorImpl implements HolderValidator{
	
	@Autowired
	private LegalAccountsHolderRepository legalRepository;
	
	@Autowired
	private PhysicalAccountHolderRepository physicalRepository;
	
	@Override
	public void validateRut(String rut) {
		
		LegalAccountsHolder legalEntity = legalRepository.findByRut(rut);
		if(legalEntity != null) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Existe una persona jurida con este rut");
		}
		PhysicalAccountHolder physicalEntity = physicalRepository.findByRut(rut);
		if(physicalEntity != null) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Existe una persona natural con este rut");
		}
		
	}

	

}
